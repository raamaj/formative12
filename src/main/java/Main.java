import java.util.ArrayList;
import java.util.List;

public class Main {
    // Create List for all possible winner
    static List<String> possibleWinner = new ArrayList<>();
    static LotteryClub lc = new LotteryClub();

    public static void main(String[] args) {
        ArrayList<Members> members = new ArrayList<>();
        members.add(new Members("Deni"));
        members.add(new Members("Rina"));
        members.add(new Members("Reni"));
        members.add(new Members("Agus"));
        members.add(new Members("Budi"));
        members.add(new Members("Dian"));
        members.add(new Members("Galuh"));
        members.add(new Members("Jejen"));
        members.add(new Members("Santi"));
        members.add(new Members("Siti"));
        members.add(new Members("Naddya"));
        members.add(new Members("Lusi"));
        members.add(new Members("Sendi"));
        members.add(new Members("Maya"));
        members.add(new Members("Balqis"));

        // Generate coupon for each member
        possibleWinner = lc.getCouponList(members);

        // Instance for winner
        Members winner = new Members();

        // Getting winner
        for (int i = 0; i < members.size(); i++){
            //Members deposit each round
            lc.setDepositMembers(members);

            // Getting the winner for each round
            String winnerCoupon = lc.getWinner(possibleWinner);

            // Checking the winner
            winner = lc.getWinner(members, winnerCoupon);
            reportWinner(winner, (i+1));

            // Delete winner from coupon list
            possibleWinner.remove(winnerCoupon);

            // Reset total deposit
            lc.totalWin = 0;
        }
    }

    public static void reportWinner(Members winner, int round){
        System.out.println("============================");
        System.out.printf("     Winner for round %d    \n", round);
        System.out.println("============================");
        System.out.println("Name : " + winner.getName());
        System.out.println("Coupon : " + winner.getCode());
        System.out.println("Amount : " + lc.totalWin);
    }
}
