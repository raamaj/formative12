public class Members {
    private String name;
    private String code;
    private int deposit;
    private boolean isWin;

    public Members(){}

    public Members(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    @Override
    public String toString() {
        return "Members{" + "name='" + name + '\'' + ", code='" + code + '\'' + ", deposit=" + deposit + ", isWin=" + isWin + '}';
    }
}
