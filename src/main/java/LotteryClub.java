import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LotteryClub {
    public int totalWin = 0;
    public String generateCoupon() {
        String allChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * allChar.length());
            salt.append(allChar.charAt(index));
        }
        return salt.toString();
    }

    public void setDepositMembers(ArrayList<Members> members){
        for (Members member : members){
            // all members deposit for each round
            member.setDeposit(150000);
            this.totalWin = this.totalWin + member.getDeposit();
        }
    }

    public Members getWinner(ArrayList<Members> members, String winnerCoupon){
        Members winner = new Members();

        for (Members member : members){
            String coupon = member.getCode();
            if (coupon.equals(winnerCoupon)){
                winner = member;
                break;
            }
        }
        return winner;
    }

    public List<String> getCouponList(ArrayList<Members> members){
        List<String> couponList = new ArrayList<>();
        for(Members member : members){
            member.setCode(this.generateCoupon());
            couponList.add(member.getCode()); // Save coupon to list
        }
        return couponList;
    }

    public String getWinner(List<String> possibleWinner){
        Random randomize = new Random();
        return possibleWinner.get(randomize.nextInt(possibleWinner.size()));
    }
}
