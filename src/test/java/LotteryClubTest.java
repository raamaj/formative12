import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LotteryClubTest {

    ArrayList<Members> members = new ArrayList<>();
    LotteryClub lc = new LotteryClub();

    @BeforeEach
    public void setUp(){
        members.add(new Members("Deni"));
        members.add(new Members("Rina"));
        members.add(new Members("Reni"));
        members.add(new Members("Agus"));
        members.add(new Members("Budi"));
        members.add(new Members("Dian"));
        members.add(new Members("Galuh"));
        members.add(new Members("Jejen"));
        members.add(new Members("Santi"));
        members.add(new Members("Siti"));
        members.add(new Members("Naddya"));
        members.add(new Members("Lusi"));
        members.add(new Members("Sendi"));
        members.add(new Members("Maya"));
        members.add(new Members("Balqis"));
    }

    @Test
    public void generateCouponTest(){
        String firstCoupon = lc.generateCoupon();
        String secondCoupon = lc.generateCoupon();
        Assertions.assertNotEquals(firstCoupon,secondCoupon);
    }

    @Test
    public void totalDepositEachRoundTest(){
        int expectedDeposit = 2250000;
        lc.setDepositMembers(members);
        Assertions.assertEquals(expectedDeposit, lc.totalWin);
    }

    @Test
    public void winnerTest(){
        List<String> couponList;
        couponList = lc.getCouponList(members);
        ArrayList<Members> winnerList = new ArrayList<>();

        for(int i = 0; i < members.size(); i++){
            lc.setDepositMembers(members);
            String winner = lc.getWinner(couponList);
            winnerList.add(lc.getWinner(members,winner));
        }

        Assertions.assertNotEquals(winnerList.get(0).getName(), winnerList.get(1).getName());
    }
}
